#pragma once

#include "Types.h"

#include <vector>
#include <string>

u32 endianReverse32 (u32 num);
void endianReverse32 (u32 *num);
float endianReversef32 (float num);
void endianReversef32 (float *num);
u16 endianReverse16 (u16 num);
void endianReverse16 (u16 *num);

enum endian_t {
    ENDIAN_BIG,
    ENDIAN_LITTLE
};

struct ByteBuffer
{
    ByteBuffer()
    {
        m_ptr = 0;
        m_endianness = ENDIAN_BIG;
    }

    //////////

    u8 peekU8()
    {
        u8 num = *(u8*)(m_storage.data() + m_ptr);

        return num;
    }

    u8 readU8()
    {
        u8 num = *(u8*)(m_storage.data() + m_ptr);
        m_ptr += sizeof(num);

        return num;
    }

    u16 readU16BE()
    {
        u16 num = *(u16*)(m_storage.data() + m_ptr);

        endianReverse16(&num);
        m_ptr += sizeof(num);

        return num;
    }

    s16 readS16BE()
    {
        s16 num = *(s16*)(m_storage.data() + m_ptr);

        endianReverse16((u16*)&num);
        m_ptr += sizeof(num);

        return num;
    }

    u32 readU32BE()
    {
        u32 num = *(u32*)(m_storage.data() + m_ptr);

        endianReverse32(&num);
        m_ptr += sizeof(num);

        return num;
    }

    s32 readS32BE()
    {
        s32 num = *(s32*)(m_storage.data() + m_ptr);

        endianReverse32((u32*)&num);
        m_ptr += sizeof(num);

        return num;
    }

    float readF32BE()
    {
        float num = *(float*)(m_storage.data() + m_ptr);

        endianReversef32(&num);
        m_ptr += sizeof(num);

        return num;
    }

    u16 readU16LE()
    {
        u16 num = *(u16*)(m_storage.data() + m_ptr);

        m_ptr += sizeof(num);

        return num;
    }

    s16 readS16LE()
    {
        s16 num = *(s16*)(m_storage.data() + m_ptr);

        m_ptr += sizeof(num);

        return num;
    }

    u32 readU32LE()
    {
        u32 num = *(u32*)(m_storage.data() + m_ptr);

        m_ptr += sizeof(num);

        return num;
    }

    s32 readS32LE()
    {
        s32 num = *(s32*)(m_storage.data() + m_ptr);

        m_ptr += sizeof(num);

        return num;
    }

    float readF32LE()
    {
        float num = *(float*)(m_storage.data() + m_ptr);
        m_ptr += sizeof(num);

        return num;
    }

    double readF64LE()
    {
        double num = *(double*)(m_storage.data() + m_ptr);
        m_ptr += sizeof(num);

        return num;
    }

    //////

    u16 readU16()
    {
        if (m_endianness == ENDIAN_BIG) {
            return readU16BE();
        } else {
            return readU16LE();
        }
    }

    s16 readS16()
    {
        if (m_endianness == ENDIAN_BIG) {
            return readS16BE();
        } else {
            return readS16LE();
        }
    }


    u32 readU32()
    {
        if (m_endianness == ENDIAN_BIG) {
            return readU32BE();
        } else {
            return readU32LE();
        }
    }

    s32 readS32()
    {
        if (m_endianness == ENDIAN_BIG) {
            return readS32BE();
        } else {
            return readS32LE();
        }
    }

    float readF32()
    {
        if (m_endianness == ENDIAN_BIG) {
            return readF32BE();
        } else {
            return readF32LE();
        }
    }

    //////

    void writeU32LE (u32 num)
    {
        u8 *fml = (u8*)&num;
        m_storage.push_back(fml[0]);
        m_storage.push_back(fml[1]);
        m_storage.push_back(fml[2]);
        m_storage.push_back(fml[3]);
    }

    void writeU32BE (u32 num)
    {
        u8 *fml = (u8*)&num;
        m_storage.push_back(fml[3]);
        m_storage.push_back(fml[2]);
        m_storage.push_back(fml[1]);
        m_storage.push_back(fml[0]);
    }

    void writeU16BE (u16 num)
    {
        u8 *fml = (u8*)&num;
        m_storage.push_back(fml[1]);
        m_storage.push_back(fml[0]);
    }

    void writeU16LE (u16 num)
    {
        u8 *fml = (u8*)&num;
        m_storage.push_back(fml[0]);
        m_storage.push_back(fml[1]);
    }

    void writeSizedString (const std::string& str)
    {
        m_storage.push_back(str.size());

        for (char c : str) {
            m_storage.push_back(c);
        }
    }

    void writeWideString (const std::vector<u16>& str)
    {
        for (u16 c : str) {
            writeU16BE(c);
        }

        m_storage.push_back(0);
        m_storage.push_back(0);
    }

    void read (void *ptr, size_t size)
    {
        memcpy(ptr, m_storage.data()+m_ptr, size);
        m_ptr += size;
    }

    //

    size_t size() const
    {
        return m_storage.size();
    }

    size_t tell() const
    {
        return m_ptr;
    }

    void seek (size_t pos)
    {
        m_ptr += pos;
    }

    void seekSet(size_t pos)
    {
        m_ptr = pos;
    }

    void seekAlignment()
    {
        m_ptr += (4 - (m_ptr % 4));
    }

    u8* data()
    {
        return m_storage.data();
    }

    void resize (size_t size)
    {
        m_storage.resize(size);
    }

    endian_t m_endianness;

private:
    std::vector<u8> m_storage;
    size_t m_ptr;
};

