#pragma once

#include "Types.h"
#include "glm/glm.hpp"

#include <string>
#include <map>
#include <SFML/Graphics/Texture.hpp>

typedef std::map<std::string, int> UniformTable;
typedef std::map<int, sf::Texture*> TextureTable;

struct Shader
{
	Shader();
	~Shader();

	void bind();
	void bindTextures();
	void unbind();

	int loadFromFile (const std::string& filename);

	void setUniform(const std::string& name, float value);
	void setUniform(const std::string& name, const glm::vec2& value);
	void setUniform(const std::string& name, const glm::vec3& value);
	void setUniform(const std::string& name, const glm::mat4& value);
	void setUniform(const std::string& name, sf::Texture *value);

	u32 vertShaderId;
	u32 fragShaderId;
	u32 programId;
	int currentTexture;

	UniformTable programUniforms;
	TextureTable programTextures;
};
