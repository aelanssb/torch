#include "Types.h"

#define SWAP_IN_PLACE(a,b) *(bytes+a) = *(((u8*)&tmp)+b)
#define REVERSE(a,b) *(((u8*)&ret)+b) = *(bytes+a)

u32 endianReverse32 (u32 num)
{
    u8 *bytes = (u8*)&num;
    u32 ret;

    REVERSE(0, 3);
    REVERSE(1, 2);
    REVERSE(2, 1);
    REVERSE(3, 0);

    return ret;
}

void endianReverse32 (u32 *num)
{
    u8 *bytes = (u8*)num;
    u32 tmp = *num;

    SWAP_IN_PLACE(0, 3);
    SWAP_IN_PLACE(1, 2);
    SWAP_IN_PLACE(2, 1);
    SWAP_IN_PLACE(3, 0);
}

float endianReversef32 (float num)
{
    u8 *bytes = (u8*)&num;
    float ret;

    REVERSE(0, 3);
    REVERSE(1, 2);
    REVERSE(2, 1);
    REVERSE(3, 0);

    return ret;
}

void endianReversef32 (float *num)
{
    u8 *bytes = (u8*)num;
    float tmp = *num;

    SWAP_IN_PLACE(0, 3);
    SWAP_IN_PLACE(1, 2);
    SWAP_IN_PLACE(2, 1);
    SWAP_IN_PLACE(3, 0);
}

u16 endianReverse16 (u16 num)
{
    u8 *bytes = (u8*)&num;
    u16 ret;

    REVERSE(0, 1);
    REVERSE(1, 0);

    return ret;
}

void endianReverse16 (u16 *num)
{
    u8 *bytes = (u8*)num;
    u16 tmp = *num;

    SWAP_IN_PLACE(0, 1);
    SWAP_IN_PLACE(1, 0);
}

#undef SWAP_IN_PLACE
#undef REVERSE
