#pragma once

#include "ByteBuffer.h"
#include "MovieClip.h"

#include <map>

struct TexListHeader
{
	char magic[4]; // "TLST"
	u16 unk0;
	u16 numAtlases;
	u16 numEntries;
	u16 unkOffset;
	u16 entriesOffset;
	u16 stringOffset;
};

struct TexListEntry
{
	u32 stringOffset;
	u32 stringOffset2; // Always same as startPos?

	// Top-left corner.
	float x1;
	float y1;
	// Bottom-right corner.
	float x2;
	float y2;

	u16 width;
	u16 height;
	u16 atlasId;

	u16 pad;
};

struct GraphicCorner
{
	GraphicCorner() {}

	explicit GraphicCorner (ByteBuffer *buf)
	{
		init(buf);
	}

	void init (ByteBuffer *buf)
	{
		pos.x = buf->readF32();
		pos.y = buf->readF32();
		uv.x = buf->readF32();
		uv.y = buf->readF32();
	}

	sf::Vector2f pos;
	sf::Vector2f uv;
};

struct DocumentProperties
{
	int fps;
	int width;
	int height;
};

struct Atlas
{
	int id;
	int unk;

	float width;
	float height;

	bool dynamic;

	sf::Texture texture;
};

struct Graphic
{
	int atlasId;
	std::string name;

	const sf::Texture *texture;

	std::vector<sf::Vertex> vertices;
};

struct Shape
{
	int id;
	std::vector<Graphic> graphics;
};

struct Text
{
	std::string placeholder;
	glm::vec4 color;
	float size;
};

struct Lumen
{
	int loadFromFile(const char *filename)
	{
		_font.loadFromFile("Boku2-Bold.otf");

		if (loadGraphicNames() != 0) {
			return 1;
		}

		title = filename;

		ByteBuffer buf;

		{
			FILE *file = fopen(filename, "rb");
	        if (!file) {
	            fprintf(stderr, "Could not load \"%s\"", filename);
	            return 1;
	        }

			fseek(file, 0, SEEK_END);
			u32 filesize = ftell(file);
			fseek(file, 0, SEEK_SET);

			buf.resize(filesize);
			fread(buf.data(), buf.size(), 1, file);

			fclose(file);
		}

		u32 magic = buf.readU32LE();
		if (magic != 0x424D4C) {
			fprintf(stderr, "%s is not a valid lumen file!\n", filename);
			return 1;
		}

		if (buf.readU32LE() == 0x10000000) {
			buf.m_endianness = ENDIAN_BIG;
		} else {
			buf.m_endianness = ENDIAN_LITTLE;
		}

		buf.seek(0x14);
		u32 filesize = buf.readU32();
		buf.seek(0x20);

		std::vector<glm::vec4> colors;
		std::vector<glm::mat3> transforms;
		std::vector<glm::vec2> positions;
		std::vector<Text> texts;

		_missingTexture.loadFromFile("missing.png");
		int graphicId = 0;

		bool done = false;
		while (!done) {
			u32 chunkMarker = buf.readU32();
			u32 chunkSize = buf.readU32();

			switch (chunkMarker) {
				// Error case. If you fuck up a file, you'll probably get this.
				case 0x0000: {
					fprintf(stderr, "PANIC! Null chunk\n");
					exit(1);
				} break;

				// Symbols
				case 0xF001: {
					u32 numSymbols = buf.readU32();

	                while (symbols.size() < numSymbols) {
	                    std::string symbol;

	                    u32 len = buf.readU32();

	                    if (len == 0) {
	                        buf.seek(4);
	                    } else {
	                        symbol.resize(len);
	                        buf.read((char*)symbol.data(), symbol.size());
	                        buf.seekAlignment();
	                    }

	                    symbols.push_back(std::move(symbol));
	                }
				} break;

				// Colors
				case 0xF002: {
					u32 numColors = buf.readU32();

					for (int i = 0; i < numColors; ++i) {
						u16 r = buf.readU16();
						u16 g = buf.readU16();
						u16 b = buf.readU16();
						u16 a = buf.readU16();

						if (r == 0) r = 255;
						if (g == 0) g = 255;
						if (b == 0) b = 255;
						if (a == 0) a = 255;

						colors.emplace_back(
							(float)r/255.f,
							(float)g/255.f,
							(float)b/255.f,
							(float)a/255.f
						);
					}
				} break;

				// Matrices
				case 0xF003: {
	                u32 numEntries = buf.readU32();

	                for (int i = 0; i < numEntries; ++i) {
	                    float a = buf.readF32();
	                    float b = buf.readF32();
	                    float c = buf.readF32();
	                    float d = buf.readF32();
	                    float tx = buf.readF32();
	                    float ty = buf.readF32();

	                    transforms.emplace_back(
	                    	a, c, tx,
	                    	b, d, ty,
	                    	0.f, 0.f, 1.f
	                    );
	                }
				} break;

				// Positions
				case 0xF103: {
					u32 numPositions = buf.readU32();

					for (int i = 0; i < numPositions; ++i) {
						float x = buf.readF32();
						float y = buf.readF32();

						positions.emplace_back(x, y);
					}
				} break;

				// Properties
				case 0xF00C: {
					buf.seek(0x1C);
					properties.fps = buf.readF32();
					properties.width = buf.readF32();
					properties.height = buf.readF32();
					buf.seek(0x08);

					printf("fps: %d, width: %d, height: %d\n",
						properties.fps,
						properties.width,
						properties.height
					);
				} break;

				// Atlases
				case 0xF007: {
					u32 numAtlases = buf.readU32();

					for (int i = 0; i < numAtlases; ++i) {
						Atlas atlas;
						atlas.id = buf.readU32();
						atlas.unk = buf.readU32();
						atlas.width = buf.readF32();
						atlas.height = buf.readF32();
						atlas.dynamic = false;

						std::string pathHack = STUFF_PATH;
						char texNameBuf[13];
						sprintf(texNameBuf, "img-%05d.png", atlas.id);
						pathHack += texNameBuf;

						if (!atlas.texture.loadFromFile(pathHack.c_str())) {
							atlas.texture = _missingTexture;
							atlas.dynamic = true;
						}
						atlas.texture.setSmooth(true);
						atlases.push_back(std::move(atlas));
					}
				} break;

				// Shape and graphics
				case 0xF022: {
					Shape shape;
					shape.id = buf.readU32();

					buf.readU32(); // unk 0
					u32 texlistEntry = buf.readU32();
					buf.readU32(); // unk 2

					u32 numGraphics = buf.readU32();

					for (int graphicIdx = 0; graphicIdx < numGraphics; ++graphicIdx) {
						buf.readU32(); // chunk marker
						buf.readU32(); // chunk size

						Graphic graphic;
						graphic.name = "";//graphicNames[texlistEntry];
						graphic.atlasId = buf.readU32();
						buf.readU16(); // unk 0
						u32 numVerts = buf.readU16();
						u32 numIndices = buf.readU32();

						graphic.texture = &atlases[graphic.atlasId].texture;
						auto size = graphic.texture->getSize();

						std::vector<sf::Vertex> verts;
						for (int i = 0; i < numVerts; ++i) {
							sf::Vertex vert;
							vert.position.x = buf.readF32();
							vert.position.y = buf.readF32();
							vert.texCoords.x = buf.readF32() * size.x;
							vert.texCoords.y = buf.readF32() * size.y;

							verts.push_back(std::move(vert));
						}

						for (int i = 0; i < numIndices; ++i) {
							graphic.vertices.push_back(verts[buf.readU16()]);
						}

						// indices are padded to word boundaries
		                if ((numIndices % 2) != 0) {
		                    buf.readU16();
		                }

						shape.graphics.push_back(std::move(graphic));
					}

					shapes.push_back(std::move(shape));
				} break;

				// MovieClips and children
				case 0x0027: {
					MovieClip mc;
					mc.lumen = this;

					mc.id = buf.readU32(); // unk 0
					buf.readU32(); // unk 1
					buf.readU32(); // unk 2

					u32 numLabels = buf.readU32();
					u32 numFrames = buf.readU32();
					u32 numKeyframes = buf.readU32();

					buf.readU32(); // unk 3

					for (int labelId = 0; labelId < numLabels; ++labelId) {
						buf.readU32(); // marker
						buf.readU32(); // size

		                Label label;
		                label.name = symbols[buf.readU32()];
		                label.startFrame = buf.readU32();
		                label.unk = buf.readU32();

		                mc.labels.push_back(std::move(label));
					}

					u32 framesToRead = numFrames + numKeyframes;

					for (int frameId = 0; frameId < framesToRead; ++frameId) {
						u32 frameType = buf.readU32();
						buf.readU32(); // size

						Frame frame;
						frame.id = buf.readU32();
						u32 numChildren = buf.readU32();

						for (int childId = 0; childId < numChildren; ++childId) {
							u32 childMarker = buf.readU32();
							u32 childSize = buf.readU32();

							// Object placement
							if (childMarker == 0x0004) {
								Placement placement;
								placement.objectId = buf.readU32();
								placement.placementId = buf.readU32();
								buf.readU32(); // unk
								placement.name = symbols[buf.readU32()].c_str();
								buf.readU16(); // flags 1
								buf.readU16(); // flags 2
								placement.mcObjectId = buf.readU16();
								buf.readU16(); // flags 4

								u16 transformFlags = buf.readU16();
								u16 transformId = buf.readU16();
								// // No clue if this is correct, but it doesn't seem
								// // to make a difference.
								// if (transformFlags != 0) {
								// 	placement.transform = transforms[transformId];
								// }

								s16 positionFlags = buf.readU16(); // flags 5
								s16 positionId = buf.readS16();
								if (positionId != -1) {
									if (positionFlags & 0x8000) {
										placement.position = positions[positionId];
									} else {
										placement.transform = transforms[positionId];
									}
								}

								buf.readS32();
								s32 colorId = buf.readS32(); // colorId
								if (colorId == -1) {
									placement.hasColor = false;
								} else {
									placement.hasColor = true;
									placement.color = colors[colorId];
								}

								u32 numF037s = buf.readU32();
								u32 numF014s = buf.readU32();

								for (int i = 0; i < numF037s; ++i) {
									buf.readU32(); // marker
									u32 fsize = buf.readU32();
									buf.seek(fsize * 4);
								}

								for (int i = 0; i < numF014s; ++i) {
									buf.readU32(); // marker
									u32 fsize = buf.readU32();
									buf.seek(fsize * 4);
								}

								frame.placements.push_back(std::move(placement));
							} else if (childMarker == 0x0005) {
								buf.readU32();
								frame.deletions.push_back(buf.readU16());
								buf.readU16();
							} else if (childMarker == 0x000C) {
								buf.readU32();
								buf.readU32();
							} else if (childMarker == 0x0000) {
								printf("null child!!\n");
								exit(1);
								// printf("f104: %d %d\n", buf.readU32(), buf.readU32());
							} else {
								fprintf(stderr, "0x%04X: YOU FUCKED UP OR DIDN'T SUPPORT SOMETHING\n", buf.tell());
								buf.seek(childSize * 4);

							}
						}

						if (frameType == 0xF105) {
							mc.keyframes.push_back(std::move(frame));
						} else {
							mc.frames.push_back(std::move(frame));
						}
					}

					movieclips.push_back(std::move(mc));
				} break;

				case 0xFF00: {
					done = true;
				} break;

				default: {
					buf.seek(chunkSize * 4);
				} break;
			}
		}

		parseMovieClipNames();

		return 0;
	}

	int loadGraphicNames()
	{
		ByteBuffer buf;

		{
			FILE *file = fopen(STUFF_PATH "texlist.lst", "rb");
			if (!file) {
				fprintf(stderr, "Failed to load texlist! PANICCCC!!!\n");
				return 1;
			}

			fseek(file, 0, SEEK_END);
			buf.resize(ftell(file));
			fseek(file, 0, SEEK_SET);

			fread(buf.data(), buf.size(), 1, file);

			fclose(file);
		}

		// TODO: check if 3ds/pokken use LE
		buf.m_endianness = ENDIAN_BIG;

		u32 magic = buf.readU32();
		buf.readU16(); // unk
		buf.readU16(); // numAtlases
		u16 numEntries = buf.readU16();
		buf.readU16(); // unk offset
		buf.readU16(); // entries offset
		u16 strOffset = buf.readU16();

		int offs = 0;
		for (int i = 0; i < numEntries; ++i) {
			std::string zzz((const char*)(buf.data() + strOffset + offs));
			offs += zzz.size() + 1;

			graphicNames.push_back(zzz);
		}

		return 0;
	}

	// TODO: make less hacky
	void parseMovieClipNames()
	{
		for (auto& mc : movieclips) {
			for (auto& keyframe : mc.keyframes) {
				for (auto& placement : keyframe.placements) {
					MovieClip *childMc = getMovieClipById(placement.objectId);

					if (strlen(placement.name) == 0) {
						continue;
					}

					if (childMc && childMc->name.size() == 0) {
						childMc->name = placement.name;
					}
				}
			}
		}
	}

	Shape* getShapeById (u32 id)
	{
		for (auto& shape : shapes) {
			if (shape.id == id) {
				return &shape;
			}
		}

		return nullptr;
	}

	MovieClip* getMovieClipById (u32 id)
	{
		for (auto& mc : movieclips) {
			if (mc.id == id) {
				return &mc;
			}
		}

		return nullptr;
	}

	std::string title;
	DocumentProperties properties;

	std::vector<std::string> symbols;
	std::vector<Shape> shapes;
	std::vector<Atlas> atlases;
	std::vector<MovieClip> movieclips;

	std::vector<std::string> graphicNames;

	sf::Font _font;
	sf::Texture _missingTexture;
};
