#include <cstdio>
#include <string>
#include <vector>

#define GLEW_STATIC
#define SFML_STATIC

#include <GL/glew.h>
#include <SFML/Window.hpp>

// #include "Constants.h"
// #include "Lumen.h"
// #include "SFML.h"
// #include "Shader.h"

// #include "imgui.h"
// #include "imgui_internal.h"
// #include "imgui-SFML.h"

#include "Types.h"
#include "soil.h"

// // hack to stop crashing...
// sf::Texture *bgTex;

#include "Texture.h"

void initGL()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.f, 640.f, 480.f, 0.f, 0.f, 1.f);
	glMatrixMode(GL_MODELVIEW);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_TEXTURE_2D);
}

struct Shape
{

};

int main (int argc, char *argv[])
{
	sf::Window win(sf::VideoMode(512, 512), "Torch");

	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "failed to init glew. uhhh idk\n");
		return 1;
	}

	initGL();

	Texture texture;
	if (!texture.loadFromGtxFile("test.gtx")) {
		printf("failed to load texture\n");
		return 1;
	}

	while (win.isOpen()) {
		sf::Event ev;
		while (win.pollEvent(ev)) {
			if (ev.type == sf::Event::Closed) {
				win.close();
			}

			if (ev.type == sf::Event::KeyPressed) {
				if (ev.key.code == sf::Keyboard::Escape) {
					win.close();
				}
			}
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glColor4f(1.f, 1.f, 1.f, 1.f);

		texture.bind();

		glBegin(GL_TRIANGLES);
			glTexCoord2f(0.f, 0.f);
			glVertex3f(0.f, 0.f, 0.f);
			glTexCoord2f(1.f, 0.f);
			glVertex3f(512.f, 0.f, 0.f);
			glTexCoord2f(1.f, 1.f);
			glVertex3f(512.f, 512.f, 0.f);

			glTexCoord2f(1.f, 1.f);
			glVertex3f(512.f, 512.f, 0.f);
			glTexCoord2f(0.f, 1.f);
			glVertex3f(0.f, 512.f, 0.f);
			glTexCoord2f(0.f, 0.f);
			glVertex3f(0.f, 0.f, 0.f);
		glEnd();

		win.display();
	}

	return 0;
}

#ifdef NVM

int mainzzz (int argc, char *argv[])
{
	Lumen lumen;
	lumen.loadFromFile(STUFF_PATH FILEHACK ".lm");

	auto windowSize = sf::Vector2u(1280, 720);
	sf::RenderWindow win(sf::VideoMode(windowSize.x, windowSize.y), "Torch");
	sf::RenderTexture renderTarget;
	renderTarget.create(lumen.properties.width, lumen.properties.height);

	ImGui::SFML::Init(win);

	std::string title = "Torch - [";
	title += lumen.title;
	title += "]";
	win.setTitle(title.c_str());
	win.setFramerateLimit(lumen.properties.fps);

	win.setSize(windowSize);
	win.setPosition(sf::Vector2i(363, 204));

	bool useOriginHack = true;
	sf::Transform originHack;
	originHack.translate(lumen.properties.width / 2, lumen.properties.height / 2);

	bgTex = new sf::Texture;
	bgTex->loadFromFile("background.png");
	bgTex->setRepeated(true);

	sf::Sprite bgSprite;
	bgSprite.setTexture(*bgTex);
	bgSprite.setTextureRect(sf::IntRect(0, 0, lumen.properties.width, lumen.properties.height));

	bool fullscreen = false;
	bool settingFullscreen = false;
	int currentMcId = 0;
	MovieClipInstance currentMc;
	currentMc.tpl = &lumen.movieclips[currentMcId];

	sf::Clock deltaClock;
	while (win.isOpen()) {
		ImGuiIO& io = ImGui::GetIO();

		sf::Event ev;
		while (win.pollEvent(ev)) {
			ImGui::SFML::ProcessEvent(ev);

			if (ev.type == sf::Event::Closed) {
				win.close();
			}

			if (ev.type == sf::Event::Resized || settingFullscreen) {
				windowSize.x = ev.size.width;
				windowSize.y = ev.size.height;

				settingFullscreen = false;
				win.create(sf::VideoMode(windowSize.x, windowSize.y), title.c_str(), (fullscreen ? sf::Style::Fullscreen : sf::Style::Resize|sf::Style::Close));
			}

			if (ev.type == sf::Event::KeyPressed) {
				int lastMcId = currentMcId;

				if (ev.key.code == sf::Keyboard::Escape) {
					win.close();
				}

				if (ev.key.code == sf::Keyboard::Down) {
					--currentMcId;
				}

				if (ev.key.code == sf::Keyboard::Up) {
					++currentMcId;
				}

				if (ev.key.code == sf::Keyboard::Space) {
					currentMc.togglePlayback();
				}

				if (ev.key.code == sf::Keyboard::BackSpace) {
					auto playState = currentMc.playing;
					currentMc.reset();
					currentMc.playing = playState;
				}

				if (ev.key.code == sf::Keyboard::F11) {
					win.create(sf::VideoMode(windowSize.x, windowSize.y), title.c_str(), sf::Style::Fullscreen);
				}

				if (currentMcId < 0) {
					currentMcId = lumen.movieclips.size()-1;
				}

				currentMcId %= lumen.movieclips.size();

				if (lastMcId != currentMcId) {
					currentMc.reset();
					currentMc.tpl = &lumen.movieclips[currentMcId];
				}
			}
		}

		ImGui::SFML::Update(deltaClock.restart());

		const char *label = currentMc.tpl->getLabelForFrameId(currentMc.currentFrame);
		currentMc.update();

		// Configure styles
		auto& style = ImGui::GetStyle();
		style.WindowRounding = 0.f;
		style.Colors[ImGuiCol_WindowBg] = ImVec4(0.2f, 0.2f, 0.2f, 1.f);

		// ImGui::ShowTestWindow();

		int menuHeight;

		// Menu bar
		if (ImGui::BeginMainMenuBar()) {
			menuHeight = ImGui::GetWindowHeight();
		    if (ImGui::BeginMenu("File")) {
		        if (ImGui::MenuItem("Quit", "CTRL+W")) {
		        	win.close();
		        }
		        ImGui::EndMenu();
		    }

		    // NYI
		    if (ImGui::BeginMenu("Edit")) {
		        if (ImGui::MenuItem("Origin Hack", NULL, &useOriginHack)) {}
		        ImGui::Separator();
		        if (ImGui::MenuItem("Undo", "CTRL+Z")) {}
		        if (ImGui::MenuItem("Redo", "CTRL+Y", false, false)) {}  // Disabled item
		        ImGui::Separator();
		        if (ImGui::MenuItem("Cut", "CTRL+X")) {}
		        if (ImGui::MenuItem("Copy", "CTRL+C")) {}
		        if (ImGui::MenuItem("Paste", "CTRL+V")) {}
		        ImGui::EndMenu();
		    }
		    ImGui::EndMainMenuBar();
		}

		int outlineWidth;
		int timelineHeight = 110;
		// Outline
		ImGui::SetNextWindowSizeConstraints(ImVec2(0, -1), ImVec2(FLT_MAX, -1));
		ImGui::Begin("Outline", NULL, ImGuiWindowFlags_NoCollapse|ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoTitleBar);
		{
			int height = windowSize.y - timelineHeight - menuHeight;
			ImGui::SetWindowSize(ImVec2(ImGui::GetWindowWidth(), height));
			ImGui::SetWindowPos(ImVec2(0, menuHeight));

			if (ImGui::TreeNode("Texture Atlases")) {
				for (int i = 0; i < lumen.atlases.size(); ++i) {
					auto& atlas = lumen.atlases[i];

					if (atlas.dynamic) {
						std::string name = "Dynamic ";
						name += std::to_string(i);
						ImGui::BulletText(name.c_str());
					} else {
						char name[13];
						sprintf(name, "img-%05d.nut", i);
						ImGui::Bullet();
						ImGui::Selectable(name);
					}
				}
				ImGui::TreePop();
			}

			if (ImGui::TreeNode("Graphics")) {
				for (auto& shape : lumen.shapes) {
					for (auto& graphic : shape.graphics) {
						ImGui::Bullet();
						ImGui::Selectable(graphic.name.c_str());
					}
				}
				ImGui::TreePop();
			}

			if (ImGui::TreeNode("MovieClips")) {
				for (int i = 0; i < lumen.movieclips.size(); ++i) {
					std::string name;

					if (lumen.movieclips[i].name.size() > 0) {
						name = lumen.movieclips[i].name;
					} else {
						name = "MovieClip ";
						name += std::to_string(i);
					}

					ImGui::Bullet();
					if (ImGui::Selectable(name.c_str(), currentMcId == i)) {
						currentMcId = i;
						currentMc.reset();
						currentMc.tpl = &lumen.movieclips[currentMcId];
					}
				}
				
				ImGui::TreePop();
			}

			outlineWidth = ImGui::GetWindowWidth();
		}
		ImGui::End();


		// Properties
		int properiesWidth;
		if (ImGui::Begin("properties", NULL, ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoMove))
		{
			properiesWidth = 250;
			int height = windowSize.y - timelineHeight - menuHeight;
			ImGui::SetWindowSize(ImVec2(properiesWidth, height));
			ImGui::SetWindowPos(ImVec2(windowSize.x - properiesWidth, menuHeight));

			if (ImGui::TreeNode("Active Objects")) {
				int i = 0;
				for (auto& kv : currentMc.activeObjects) {
					auto& obj = kv.second;
					ImGui::PushID(i);
					{
						const char *name;
						const char *type = (obj.type == OBJECT_GRAPHIC) ? "Graphic" : "MovieClip";

						if (strlen(obj.name) > 0) {
							name = obj.name;
						} else if (obj.type == OBJECT_GRAPHIC) {
							name = obj.graphics[0]->name.c_str();
						}

						if (ImGui::TreeNodeEx("%s: %s %i", ImGuiTreeNodeFlags_DefaultOpen, name, type, i)) {
							glm::vec4 col = obj.color;
							float zzz[4] = { col.x, col.y, col.z, col.w };
							ImGui::ColorEdit4("Color", zzz);
							obj.color.x = zzz[0];
							obj.color.y = zzz[1];
							obj.color.z = zzz[2];
							obj.color.w = zzz[3];

							ImGui::Text("");
							ImGui::TreePop();
						}
						
					}
					ImGui::PopID();

					++i;
				}

				ImGui::TreePop();
			}

		} else {
			properiesWidth = 0;
		}
		ImGui::End();

		// Timeline
		int timelineFlags = ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoCollapse|ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoTitleBar;
		ImGui::Begin("timeline", NULL, timelineFlags);
		{
			int yPos = windowSize.y - timelineHeight;
			ImGui::SetWindowSize(ImVec2(windowSize.x, timelineHeight));
			ImGui::SetWindowPos(ImVec2(0, yPos));

			ImDrawList *drawList = ImGui::GetWindowDrawList();

			ImColor normalLine(0x7F, 0x7F, 0x7F, 0xFF);
			ImColor markedLine(0xFF8855FF);
			ImColor currentLine(0x90, 0x20, 0xC0, 0xFF);
			ImColor keyframeLine(0x50, 0xC0, 0x40, 0xFF);

			float y = windowSize.y - timelineHeight + 25;

			for (int i = 0; i < 200; ++i) {

				if ((i % 5) == 0) {
					if (i != 0) {
						char numText[4];
						sprintf(numText, "%d", i);
						drawList->AddText(ImVec2(i * 8 - 8, y-16), markedLine, numText);
					}

					drawList->AddLine(ImVec2(i * 8, y), ImVec2(i * 8, y + 50), markedLine, 1);
				} else {
					drawList->AddLine(ImVec2(i * 8, y), ImVec2(i * 8, y + 50), normalLine, 1);
				}
			}

			for (int i = 0; i < currentMc.tpl->labels.size(); ++i) {
				auto& label = currentMc.tpl->labels[i];

				drawList->AddLine(ImVec2(label.startFrame * 8, y), ImVec2(label.startFrame * 8, y + 65), keyframeLine, 2);

				ImRect bb(label.startFrame * 8 - 2, y, label.startFrame * 8 + 3, y + 65);
				ImGui::ItemAdd(bb, NULL);

		        if (ImGui::IsItemHoveredRect()) {
		            ImGui::SetTooltip(label.name.c_str());

		            if (io.MouseDown[0]) {
		            	currentMc.jumpToKeyframe(i);
		            }
		        }
			}

			int x = currentMc.currentFrame * 8;

			drawList->AddLine(ImVec2(x, y), ImVec2(x, y + 55), currentLine, 1);		
			drawList->AddLine(ImVec2(x+3, y-12), ImVec2(x+3, y), currentLine, 6);

			timelineHeight = ImGui::GetWindowHeight();
		}
		ImGui::End();

		win.clear();
		renderTarget.draw(bgSprite);

		if (useOriginHack) {
			renderTarget.draw(currentMc, originHack);
		} else {
			renderTarget.draw(currentMc);
		}
		renderTarget.display();

		const sf::Texture& texture = renderTarget.getTexture();
		sf::Sprite sprite(texture);
		auto windowPos = sf::Vector2f(outlineWidth, menuHeight);
		sprite.setPosition(windowPos);

		auto scale = sf::Vector2f(
			(float)(windowSize.x - windowPos.x - properiesWidth) / lumen.properties.width,
			(float)(windowSize.y - windowPos.y - timelineHeight) / lumen.properties.height
		);
		sprite.setScale(scale);

		// auto scale = sf::Vector2f(
		// 	(float)(windowSize.x) / lumen.properties.width,
		// 	(float)(windowSize.y) / lumen.properties.height
		// );
		// sprite.setScale(scale);

		win.draw(sprite);

		ImGui::Render();
		win.display();
	}

	// ImGui::SFML::Shutdown();

	return 0;
}
#endif