#include <cstdio>
#define GLEW_STATIC
#include <GL/glew.h>
#include <SFML/OpenGL.hpp>

#include "Shader.h"

namespace {
	std::string getShaderLog (GLuint obj)
	{
		std::string log;

		GLint status;
		glGetShaderiv(obj, GL_COMPILE_STATUS, &status);

		if (!status) {
			GLint logLength;
			glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &logLength);

			if (logLength > 0) {
				log.resize(logLength);

				glGetShaderInfoLog(obj, logLength, NULL, (GLchar*)log.data());
			}
		}

		return log;
	}

	std::string getFileContents (const std::string& filename)
	{
		std::string contents;

		FILE *file = fopen(filename.c_str(), "rb");
		if (!file) {
			return "COULD NOT LOAD SHADER FILE LOL";
		}

		fseek(file, 0, SEEK_END);
		u32 fileSize = ftell(file);
		fseek(file, 0, SEEK_SET);

		contents.resize(fileSize);
		fread((void*)contents.data(), contents.size(), 1, file);

		fclose(file);

		return contents;
	}
}

Shader::Shader()
{
	auto test = getFileContents("test.txt");

	printf("%s\n", test.c_str());
}

Shader::~Shader()
{
	glDeleteProgram(programId);
	glDeleteShader(vertShaderId);
	glDeleteShader(fragShaderId);
}

int Shader::loadFromFile (const std::string& filename)
{
	vertShaderId = 0;
	fragShaderId = 0;
	programId = 0;
	currentTexture = -1;

	/// NYI!!
	return 1;
}

void Shader::bind()
{
	if (programId) {
		glUseProgram(programId);

		bindTextures();

		if (currentTexture != -1) {
			glUniform1i(currentTexture, 0);
		}
	}
}

void Shader::bindTextures()
{
	
}

void Shader::setUniform(const std::string& name, float value)
{

}

void Shader::setUniform(const std::string& name, const glm::vec2& value)
{

}

void Shader::setUniform(const std::string& name, const glm::vec3& value)
{

}

void Shader::setUniform(const std::string& name, const glm::mat4& value)
{

}

void Shader::setUniform(const std::string& name, sf::Texture *value)
{

}
