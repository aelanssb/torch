#pragma once

#include "ByteBuffer.h"

struct GFDHeader
{
	char magic[4];
	u32 size;
	u32 majorVersion;
	u32 minorVersion;
	u32 gpuVersion;
	u32 alignMode;
	u32 reserved1;
	u32 reserved2;
};

struct GFDBlockHeader
{
	char magic[4];
	u32 size;
	u32 majorVersion;
	u32 minorVersion;
	u32 type;
	u32 dataSize;
	u32 id;
	u32 typeId;
};

struct GFDSurface
{
	u32 dim;
	u32 width;
	u32 height;
	u32 depth;
	u32 numMips;
	u32 format;
	u32 aa;
	u32 use;
	u32 imageSize;
	u32 imagePtr;
	u32 mipSize;
	u32 mipPtr;
	u32 tileMode;
	u32 swizzle;
	u32 alignment;
	u32 pitch;
};

struct Texture
{
	Texture()
	{
		texId = 0;
	}

	int loadFromNutFile (const std::string& filename)
	{
		fprintf(stderr, "NYI: loadFromNutFile\n");
		return 0;
	}

	int loadFromGtxFile (const std::string& filename)
	{
		ByteBuffer buf;

		{
			FILE *file = fopen(filename.c_str(), "rb");
			if (!file) {
				fprintf(stderr, "Failed to load %s\n", filename.c_str());
				return 0;
			}

			fseek(file, 0, SEEK_END);
			buf.resize(ftell(file));
			fseek(file, 0, SEEK_SET);

			fread(buf.data(), buf.size(), 1, file);

			fclose(file);
		}

		buf.m_endianness = ENDIAN_BIG;

		GFDHeader header;
		buf.readU32(); // magic
		header.size = buf.readU32();
		header.majorVersion = buf.readU32();
		header.minorVersion = buf.readU32();
		header.gpuVersion = buf.readU32();
		header.alignMode = buf.readU32();
		header.reserved1 = buf.readU32();
		header.reserved2 = buf.readU32();

		u32 dataSize;
		u32 dataOffs;
		GFDSurface surface;

		while (buf.tell() < buf.size()) {
			GFDBlockHeader blkHeader;
			buf.readU32(); // magic
			blkHeader.size = buf.readU32();
			blkHeader.majorVersion = buf.readU32();
			blkHeader.minorVersion = buf.readU32();
			blkHeader.type = buf.readU32();
			blkHeader.dataSize = buf.readU32();
			blkHeader.id = buf.readU32();
			blkHeader.typeId = buf.readU32();
			
			if (blkHeader.type == 0x0B) {
				printf("0x0B @ 0x%02X\n", buf.tell());
				surface.dim = buf.readU32();
				surface.width = buf.readU32();
				surface.height = buf.readU32();
				surface.depth = buf.readU32();
				surface.numMips = buf.readU32();
				surface.format = buf.readU32();
				surface.aa = buf.readU32();
				surface.use = buf.readU32();
				surface.imageSize = buf.readU32();
				surface.imagePtr = buf.readU32();
				surface.mipSize = buf.readU32();
				surface.mipPtr = buf.readU32();
				surface.tileMode = buf.readU32();
				surface.swizzle = buf.readU32();
				surface.alignment = buf.readU32();
				surface.pitch = buf.readU32();

				buf.seek(0x58); // ????
				printf("read format %02X\n", surface.format);
			} else if (blkHeader.type == 0x0C) {
				dataSize = blkHeader.dataSize;
				printf("0x0C @ 0x%02X; size=0x%02X\n", buf.tell(), blkHeader.dataSize);
				dataOffs = buf.tell();

				break;
			} else {
				buf.seek(blkHeader.dataSize);
			}
		}

		ByteBuffer outBuffer;
		outBuffer.m_endianness = ENDIAN_LITTLE;

		// required fields
		u32 flags = (0x01 | 0x02 | 0x04 | 0x1000);

		// if has mipmaps
		bool mips = true;
		if (mips) {
			flags |= 0x20000;
		}

		// pitch provided for compressed texture
		flags |= 0x80000;

		outBuffer.writeU32LE(0x20534444); // "DDS "
		outBuffer.writeU32LE(124); // dwSize
		outBuffer.writeU32LE(flags); // flags
		outBuffer.writeU32LE(surface.height);
		outBuffer.writeU32LE(surface.width);
		outBuffer.writeU32LE(surface.pitch);
		outBuffer.writeU32LE(surface.depth);
		outBuffer.writeU32LE(surface.numMips);

		// dwReserved1
		for (int i = 0; i < 11; ++i) {
			outBuffer.writeU32LE(0);
		}

		// DDS_PIXELFORMAT
		outBuffer.writeU32LE(0x20); // size
		outBuffer.writeU32LE(0x01 | 0x04); // has alpha + compressed
		outBuffer.writeU32LE(894720068); // DXT5
		outBuffer.writeU32LE(0);
		outBuffer.writeU32LE(0);
		outBuffer.writeU32LE(0);
		outBuffer.writeU32LE(0);
		outBuffer.writeU32LE(0);

		u32 caps = 0x1000;

		if (mips) {
			caps |= (0x400000 | 0x08);
		}
		outBuffer.writeU32LE(caps);
		outBuffer.writeU32LE(0); // caps2
		outBuffer.writeU32LE(0); // caps3
		outBuffer.writeU32LE(0); // caps4
		outBuffer.writeU32LE(0); // reserved2

		u32 zzz = outBuffer.size();

		{
			FILE *outFile = fopen("out.dds", "wb");
			fwrite(outBuffer.data(), outBuffer.size(), 1, outFile);
			fclose(outFile);
		}

		outBuffer.resize(zzz + dataSize);
		memcpy(outBuffer.data()+zzz, buf.data()+dataOffs, dataSize);

		u8 *data = SOIL_load_image_from_memory(
			outBuffer.data(), outBuffer.size(),
			&width, &height, &channels,
			0
		);

		texId = SOIL_create_OGL_texture(data, width, height, channels, 0, 0);

		SOIL_free_image_data(data);

		return texId;
	}

	int loadFromDDSFile (const std::string& filename)
	{
		std::vector<u8> buf;

		{
			FILE *file = fopen(filename.c_str(), "rb");
			if (!file) {
				fprintf(stderr, "Failed to load %s\n", filename.c_str());
				return 0;
			}

			fseek(file, 0, SEEK_END);
			buf.resize(ftell(file));
			fseek(file, 0, SEEK_SET);

			fread(buf.data(), buf.size(), 1, file);

			fclose(file);
		}

		u8 *data = SOIL_load_image_from_memory(
			buf.data(), buf.size(),
			&width, &height, &channels,
			0
		);

		texId = SOIL_create_OGL_texture(data, width, height, channels, 0, 0);

		SOIL_free_image_data(data);

		return texId;
	}

	void bind()
	{
		glBindTexture(GL_TEXTURE_2D, texId);
	}

	u32 texId;
	int width;
	int height;
	int channels;
};
