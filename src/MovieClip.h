#pragma once

#include "Types.h"
#include "SFML.h"
#include "glm/vec2.hpp"
#include "glm/vec4.hpp"
#include "glm/mat3x3.hpp"

#include <memory>

struct Lumen;

struct Placement
{
	int objectId;
	int placementId;
	const char *name;

	u16 mcObjectId;

	bool hasColor;

	glm::mat3 transform;
	glm::vec2 position;
	glm::vec4 color;
};

struct Frame
{
	int id;

	const char *label;
	std::vector<int> deletions;
	std::vector<Placement> placements;
};

enum object_t {
	OBJECT_GRAPHIC,
	OBJECT_MOVIECLIP,
	OBJECT_UNKNOWN
};

struct Graphic;
struct MovieClipInstance;

struct ActiveObject
{
	ActiveObject()
	{
		type = OBJECT_UNKNOWN;
		name = nullptr;
	}

	object_t type;
	const char *name;

	glm::mat3 transform;
	glm::vec2 position;
	glm::vec4 color;

	// OBJECT_MOVIECLIP
	std::unique_ptr<MovieClipInstance> mc;
	// OBJECT_GRAPHIC
	std::vector<Graphic*> graphics;
	// OBJECT_DYNAMICTEXT
	// std::vector<DynamicTextInstance> text;
};

struct Label
{
	std::string name;
	u32 startFrame;
	u32 unk;
};

struct MovieClip
{
	const char* getLabelForFrameId (int frameId) const;

	u32 id;
	std::string name;

	std::vector<Label> labels;
	std::vector<Frame> frames;
	std::vector<Frame> keyframes;

	Lumen *lumen;
};

struct RenderState
{
	glm::mat4 transform;
	glm::vec4 color;

	// HACK
	float depth;
};

struct MovieClipInstance : public sf::Drawable
{
	MovieClipInstance();

	virtual void draw (sf::RenderTarget& target, sf::RenderStates states) const;
	void draw (RenderState state) const;
	void update();
	void togglePlayback();
	void stop(bool recursive = true);
	void reset();
	void jumpToKeyframe(int i);

	MovieClip *tpl;

	std::map<u16, ActiveObject> activeObjects;

	int currentFrame;
	bool playing;

private:
	void _processFrame(const Frame& frame);
};
