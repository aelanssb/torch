#include "Constants.h"
#include "MovieClip.h"
#include "Lumen.h"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include <SFML/OpenGL.hpp>

MovieClipInstance::MovieClipInstance()
{
	currentFrame = 0;
	playing = true;
}

void MovieClipInstance::togglePlayback()
{
	playing = !playing;

	for (auto& kv : activeObjects) {
		if (kv.second.type == OBJECT_MOVIECLIP) {
			kv.second.mc->togglePlayback();
		}
	}
}

void MovieClipInstance::stop (bool recursive)
{
	playing = false;

	if (recursive) {
		for (auto& kv : activeObjects) {
			if (kv.second.type == OBJECT_MOVIECLIP) {
				kv.second.mc->stop(true);
			}
		}
	}
}

void MovieClipInstance::reset()
{
	currentFrame = 0;
	playing = true;
	activeObjects.clear();
}

void MovieClipInstance::jumpToKeyframe (int i)
{
	activeObjects.clear();

	currentFrame = tpl->labels[i].startFrame;
	_processFrame(tpl->keyframes[i]);

	for (auto& kv : activeObjects) {
		if (kv.second.type == OBJECT_MOVIECLIP) {
			kv.second.mc->_processFrame(kv.second.mc->tpl->frames[0]);
		}
	}
}

void MovieClipInstance::_processFrame (const Frame& frame)
{
	if (frame.id == 0) {
		// activeObjects.clear();
	} else {
		for (u16 deletion : frame.deletions) {
			activeObjects.erase(deletion);
		}
	}

	for (auto& placement : frame.placements) {
		// Handle initial placements first.
		if (placement.mcObjectId != -1) {
			ActiveObject obj;
			Shape *childShape = tpl->lumen->getShapeById(placement.objectId);

			if (childShape) {
				obj.type = OBJECT_GRAPHIC;
				for (auto& graphic : childShape->graphics) {
					obj.graphics.push_back(&graphic);
				}
			} else {
				auto childMc = tpl->lumen->getMovieClipById(placement.objectId);

				if (childMc) {
					obj.type = OBJECT_MOVIECLIP;
					obj.mc = std::make_unique<MovieClipInstance>();
					obj.mc->tpl = childMc;
				}
			}

			if (placement.name) {
				obj.name = placement.name;
			}

			activeObjects.emplace(placement.mcObjectId, std::move(obj));
		}

		auto& obj = activeObjects[placement.mcObjectId];

		if (placement.hasColor) {
			obj.color = placement.color;
		}

		obj.transform = placement.transform;
		obj.position = placement.position;
	}

}

void MovieClipInstance::update()
{
	if (playing) {
		currentFrame %= tpl->frames.size();

		_processFrame(tpl->frames[currentFrame]);

		if (++currentFrame >= tpl->frames.size()) {
			// playing = false;
		}

		for (auto& kv : activeObjects) {
			if (kv.second.type == OBJECT_MOVIECLIP) {
				kv.second.mc->update();
			}
		}
	}
}

void MovieClipInstance::draw (RenderState state) const
{
	float depthAcc = 0.f;

	for (auto& kv : activeObjects) {
		auto& obj = kv.second;

		RenderState newstate;
		newstate.depth = state.depth + depthAcc;
		newstate.color = state.color * obj.color;
		newstate.transform = glm::translate(state.transform, glm::vec3(obj.position.x, obj.position.y, newstate.depth));
		newstate.transform *= glm::mat4(obj.transform);

		if (obj.type == OBJECT_GRAPHIC) {
			for (auto& graphic : obj.graphics) {
				if (!graphic->texture) {
					continue;
				}

				sf::Texture::bind(graphic->texture, sf::Texture::Pixels);

				glLoadMatrixf(glm::value_ptr(newstate.transform));
				glColor4fv(glm::value_ptr(newstate.color));

				glBegin(GL_TRIANGLES);
				for (auto& vert : graphic->vertices) {
					glTexCoord2f(vert.texCoords.x, vert.texCoords.y);
					glVertex2f(vert.position.x, vert.position.y);
				}
				glEnd();

				sf::Texture::bind(NULL);

			}
		} else if (obj.type == OBJECT_MOVIECLIP) {
			obj.mc->draw(newstate);
		}
	}

	depthAcc -= 0.1f;
}

void MovieClipInstance::draw (sf::RenderTarget& target, sf::RenderStates states) const
{
	target.pushGLStates();

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.f, 1920.f, 1080.f, 0.f, 0.f, 1.f);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	RenderState state;
	state.color = glm::vec4(1.f, 1.f, 1.f, 1.f);
	state.transform = glm::translate(glm::mat4(), glm::vec3(960, 540, 0));
	state.depth = 0.f;

	draw(state);

	target.popGLStates();
}

const char* MovieClip::getLabelForFrameId (int frameId) const
{
	int labelId = -1;

	for (int i = 0; i < labels.size(); ++i) {
		if (labels[i].startFrame > frameId) {
			break;
		}

		labelId = i;
	}

	if (labelId == -1) {
		return nullptr;
	}

	return labels[labelId].name.c_str();
}
