@echo off

cd "%~dp0"

if not exist bin mkdir bin
cd bin

rem SET DEBUG=1

SET SOURCES=../src/main.cpp ../src/ByteBuffer.cpp
rem ../src/MovieClip.cpp ../src/ByteBuffer.cpp ../src/imgui.cpp ../src/imgui_draw.cpp ../src/imgui-SFML.cpp ../src/Shader.cpp
SET LIBS=winmm.lib advapi32.lib gdi32.lib user32.lib opengl32.lib freetype.lib jpeg.lib glew32s.lib SOIL.lib

IF NOT DEFINED DEBUG (
	SET LIBS=%LIBS% sfml-main.lib sfml-system-s.lib sfml-window-s.lib
	SET FLAGS=-MD -EHsc
) ELSE (
	SET LIBS=%LIBS% sfml-main-d.lib sfml-system-s-d.lib sfml-window-s-d.lib sfml-graphics-s-d.lib
	SET FLAGS=-Zi -MDd -DDEBUG -EHsc
)

cl %FLAGS% -Fe"torch" %SOURCES% %LIBS% /link /NODEFAULTLIB:LIBCMT && CALL :runapp

EXIT /B %ERRORLEVEL%

:runapp
IF NOT DEFINED DEBUG (
	cd .. && bin\torch
)
